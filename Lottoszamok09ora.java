
package lottoszamok09ora;

import java.util.Scanner;

public class Lottoszamok09ora {

    public static void main(String[] args) {
        
        int[][] lottoSzamok = {
                {37,42,44,61,62},
                {18,42,54,83,89},
                {5,12,31,53,60},
                {1,28,47,56,70},
                {54,56,57,59,71},
                {7,21,33,39,86},
                {21,29,37,48,68},
                {10,21,29,40,87},
                {13,33,73,77,78},
                {2,23,65,71,84},
                {3,21,28,30,33},
                {23,31,42,73,85},
                {4,23,42,61,64},
                {17,60,66,71,85},
                {12,60,66,67,72},
                {46,50,58,62,76},
                {20,32,43,65,73},
                {55,56,58,61,71},
                {18,38,41,67,89},
                {32,41,59,66,79},
                {25,35,37,74,86},
                {1,45,60,61,82},
                {7,20,35,58,83},
                {7,37,40,46,51},
                {2,6,47,74,80},
                {1,5,22,44,88},
                {23,33,34,71,89},
                {4,56,74,77,89},
                {17,18,51,52,75},
                {7,29,30,77,80},
                {17,18,28,35,90},
                {6,24,25,53,79},
                {7,12,18,38,90},
                {25,28,45,55,74},
                {10,29,60,74,86},
                {7,24,25,50,76},
                {20,40,52,54,90},
                {16,30,81,83,87},
                {20,22,23,50,67},
                {59,68,75,80,85},
                {32,45,55,70,78},
                {13,40,55,56,76},
                {3,14,24,73,83},
                {23,25,28,66,76},
                {24,33,34,39,54},
                {12,28,34,61,70},
                {1,4,8,69,74},
                {4,15,46,49,59},
                {24,31,67,71,73},
                {12,26,36,46,49},
                {9,20,21,59,68},
                {89,24,34,11,64}
        };
        
        //  System.out.println( lottoSzamok[0].length );
        
        // 2. Rendezzük az utolsó hét számait!
        
        System.out.println("Az utolsó hét számai rendezve:");
        for ( int i=0; i<5; i++ ) {
            int minIndex= i;
            for ( int j=i; j<5; j++ ) {
                if ( lottoSzamok[51][j] < lottoSzamok[51][minIndex] ){
                    minIndex = j;
                }    
            }
            int csere = lottoSzamok[51][i];
                lottoSzamok[51][i] = lottoSzamok[51][minIndex];
                lottoSzamok[51][minIndex] = csere;
        }
        for ( int i=0; i<5; i++){
            System.out.print(lottoSzamok[51][i] + " ");
        }
        System.out.println();
        System.out.println();
        
        // 3. Kérjen be egy számot!
        
        int bekertSzam;
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Kérem adjon meg egy számot 1-51 között!");
        bekertSzam = sc.nextInt();
        
        /* 4. Írja ki a képernyőre a bekért számnak megfelelő sorszámú hét 
              lottószámait, a megkapott adatok alapján! */
        
        System.out.println("A " + bekertSzam + ". hét nyerőszámai:");
        for ( int i=0; i<5; i++ ){
            System.out.print( lottoSzamok[bekertSzam-1][i] + " ");
        }
        System.out.println();
        System.out.println();
        
        /* 5.1. A megkapott adatok alapján döntse el, hogy volt-e olyan szám,
                amit egyszer sem húztak ki az 51 hét alatt! A döntés eredményét 
                (Van/Nincs) írja ki a képernyőre! */
        
        System.out.println("Van olyan szám amit még egyszer sem húztak ki?");
        boolean volt = false;
        for ( int keresettSzam=1; keresettSzam<=90; keresettSzam++ ){
            boolean megtalaltuk = false;
            for ( int i=0; i<52; i++ ){
                for ( int j=0; j<5; j++ ){        
                    if ( lottoSzamok[i][j] == keresettSzam ){
                        megtalaltuk = true;                   
                    }
                }
            }
            if ( ! megtalaltuk ) {
                volt = true;
            }
        }
        System.out.println( volt ? "Van" : "Nincs");
        
        // 5.2. boolean tömbbel
        
        boolean[] kihuztak = new boolean[91]; // ilyenkor mind a 91 elem false
        for ( int i=0; i<52; i++ ){
            for ( int j=0; j<5; j++ ){        
                kihuztak [ lottoSzamok[i][j] ]=true;
            }
        }
        boolean talaltunk=false;
        for ( int i=1; i<91; i++ ){
            if( ! kihuztak[i] ){
                talaltunk=true;
            }
        }    
        System.out.println( talaltunk ? "Van" : "Nincs" );
        System.out.println();
        
        /* 6. A megkapott adatok alapján állapítsa meg, hogy hányszor volt 
              páratlan szám a kihúzott lottószámok között! Az eredményt a 
              képernyőre írja ki! */
        
        int counter=0;
        for ( int i=0; i<52; i++ ){
            for ( int j=0; j<5; j++ ){
                if ( lottoSzamok[i][j] % 2 == 1 ){
                    counter++;
                }
            }
        }
        System.out.println("A páratlan nyerőszámok száma az évben: " + counter );
        System.out.println();
        
        /* 7. Írja ki az összes lottószámot a képernyőre! Egy sorba egy hét 
              lottószámai kerüljenek, szóközzel elválasztva egymástól! */
        
        System.out.println("A nyerőszámok:");
        for ( int i=0; i<52; i++ ){
            for ( int j=0; j<5; j++ ){
                System.out.print( ( lottoSzamok[i][j] < 10 ? " " : "") + 
                                    lottoSzamok[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
        
        /* 8. Határozza meg az adatok alapján, hogy az egyes számokat hányszor
              húzták ki 2003-ban. Az eredményt írja ki a képernyőre a következő
              formában: az első sor első eleme az a szám legyen, ahányszor az
              egyest kihúzták! Az első sor második eleme az az érték legyen,
              ahányszor a kettes számot kihúzták stb.! (Annyit biztosan tudunk
              az értékekről, hogy mindegyikük egyjegyű.) */
        
        int[] ennyiszerHuztakKi = new int[91];  // alapból végig 0-kal van feltöltve
        
        for ( int i=0; i<52; i++ ){
            for ( int j=0; j<5; j++ ){
                ennyiszerHuztakKi[ lottoSzamok[i][j] ] ++;
            }
        }    
        System.out.println( "Ennyiszer húztuk ki az egyes számokat:");
        for ( int i=1; i<=90; i++ ){
            System.out.print( ennyiszerHuztakKi[i] + " " + ( i%10 == 0 ? "\n" : "") ) ;
        }
        System.out.println();

        /* 9. Adja meg, hogy az 1-90 közötti prímszámokból melyiket nem húzták
              ki egyszer sem az elmúlt évben! */
        
        
        for ( int i=0; i<91; i++){
            kihuztak[i] = false;
        } 
        for ( int i=0; i<52; i++ ){
            for ( int j=0; j<5; j++ ){
                kihuztak[ lottoSzamok[i][j] ] = true;
            }
        }
        System.out.print("A ki nem húzott prímszámok: ");
        for ( int szam = 1; szam < 90; szam++ ){
            if ( kihuztak[szam] == false && primTeszt(szam) ){
                System.out.print( szam + " " );
            }
        }
        System.out.println();
    }
    
    public static boolean primTeszt( int n ) {
        boolean prim = true;
        if ( n < 2 ){
            return false;
        }
        for ( int i = 2; i*i <= n; i++ ){
            if ( n % i == 0 ){
                prim = false;
                break;
            }
        }
        return prim;
    }
}
